package co.simplon.alt6;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class FirstTest {
    
    @Test
    public void exampleShouldWork() {

        int a = 4;
        int b = 5;

        int result = a+b;

        assertEquals(9, result);

    }
}
