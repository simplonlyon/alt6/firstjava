package co.simplon.alt6.oop;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;

public class TodoListTest {

    @Test
    void testShouldInstanciate() {
        TodoList list = new TodoList();

        assertNotNull(list);
    }

    @Test
    void testShouldDrawList() {

        TodoList list = new TodoList(List.of("Do something", "Do something else"));
        String result = list.draw();

        assertEquals("☐ Do something\n☐ Do something else\n", result);

    }

    @Test
    void testShouldAddTodo() {
        TodoList list = new TodoList();

        list.add("Do something");

        String result = list.draw();

        assertEquals("☐ Do something\n", result);

    }
}
