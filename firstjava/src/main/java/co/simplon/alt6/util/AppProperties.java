package co.simplon.alt6.util;

import java.io.IOException;
import java.util.Properties;
/**
 * Cette classe implémente le design pattern Singleton qui permet à une classe de ne pouvoir
 * être instanciée qu'une seule fois, pour ça on rend le constructeur privé et on crée
 * une méthode getInstance qui va créer une instance de AppProperties ou donner accès à
 * l'instance déjà existante (celle ci est stockée dans une propriété static)
 */
public class AppProperties {
    private static AppProperties instance;
    private Properties props;

    private AppProperties() {
        this.props = new Properties();
        try {
            this.props.load(AppProperties.class.getResourceAsStream("/app.properties"));
        } catch (IOException e) {
            System.out.println("App properties load error");
            e.printStackTrace();
        }
    }

    public static AppProperties getInstance() {
        if(instance == null) {
            instance = new AppProperties();
        }

        return instance;
    }

    public String getProp(String key) {
        return props.getProperty(key);
    }
}
