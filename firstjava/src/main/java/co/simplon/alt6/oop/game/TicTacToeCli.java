package co.simplon.alt6.oop.game;

import java.util.Scanner;

public class TicTacToeCli {

    private TicTacToe game;

    public TicTacToeCli() {
        game = new TicTacToe();
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Current Grid : ");
            displayGrid();

            if (!gameContinue()) {
                return;
            }

            System.out.println("In which row do you want to place ?");
            int inputRow = scanner.nextInt();
            System.out.println("In which column do you want to place ?");
            int inputCol = scanner.nextInt();

            game.place(inputRow, inputCol);

        }

    }

    private boolean gameContinue() {
        if (game.winState() == TicTacToe.X_WIN) {
            System.out.println("X won !");
            return false;
        }

        if (game.winState() == TicTacToe.O_WIN) {
            System.out.println("O won !");
            return false;
        }

        if (game.winState() == TicTacToe.DRAW) {
            System.out.println("Draw !");
            return false;
        }
        return true;
    }

    private void displayGrid() {
        for (char[] line : game.getGrid()) {
            for (char cell : line) {
                System.out.print("[" + cell + "]");
            }
            System.out.println();
        }
    }
}
