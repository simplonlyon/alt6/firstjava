package co.simplon.alt6.oop.game.gui;

import co.simplon.alt6.oop.game.TicTacToe;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class TicTacToeGUI extends Application {

    @Override
    public void start(Stage stage) {

        // TicTacToe game = new TicTacToe();


        GameGrid gameGrid = new GameGrid();
        
        gameGrid.draw();

        Scene scene = new Scene(gameGrid, 640, 480);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}