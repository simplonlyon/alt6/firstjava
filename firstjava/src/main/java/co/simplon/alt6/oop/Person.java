package co.simplon.alt6.oop;

import co.simplon.alt6.oop.interfaces.HTMLConvertable;

/**
 * Nom des classes en PascalCase
 */
public class Person implements HTMLConvertable {

    // nom des propriétés et variables en camelCase
    private String name;
    private String firstName;
    private int age;

    public int getAge() {
        return age;
    }

    public Person(String name, String firstName, int age) {
        this.name = name;
        this.firstName = firstName;
        setAge(age);
    }

    /**
     * Méthode permettant d'obtenir le nom complet d'une personne
     * 
     * @return Le prénom et le nom concaténés
     */
    public String getFullName() {
        return this.firstName + " " + this.name;
    }

    /**
     * Méthode permettant à une Person de se présenter
     */
    public void greeting() {
        System.out.println("Hello, my name is " + this.getFullName() + " I am " + this.age + " years old");
    }

    /**
     * Encapsulation des vérifications pour un changement de la propriété age,
     * on ne permet pas un age négatif
     */
    public void setAge(int age) {
        if (age < 0) {
            age = 0;
            // Ou bien si on est sévère
            throw new IllegalArgumentException("age must be positive");
        }
        this.age = age;
    }

    /**
     * Méthode de salut entre 2 personnes avec un salut différent selon
     * certains conditions
     * 
     * @param other L'autre Person à saluer
     */
    public void salute(Person other) {
        if (other == this) {
            System.out.println("forever alone");
            return;
        }
        if (other.name == this.name) {
            System.out.println("Salut la famille");
            return;
        }
        if (other.age < 40 && this.age < 40) {
            System.out.println("check");
            return;
        }
        System.out.println("salut formel");
    }

    @Override
    public String toHTML() {

        return "<article>"
                + "<h3>" + getFullName() + "</h3>"
                + "<p>age : " + age + "</p>"
                + "</article>";
    }
}
