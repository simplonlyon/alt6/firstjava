package co.simplon.alt6.oop.game;

public class TicTacToe {
    public static final Character GAME_NOT_FINISHED = null;
    public static final Character DRAW = ' ';
    public static final Character X_WIN = 'X';
    public static final Character O_WIN = 'O';

    private char[][] grid = {
            { ' ', ' ', ' ' },
            { ' ', ' ', ' ' },
            { ' ', ' ', ' ' }
    };
    private boolean playerX = true;
    private Character gameState = GAME_NOT_FINISHED;

    public TicTacToe() {
    }

    TicTacToe(char[][] initial) {
        this.grid = initial;

    }

    public char[][] getGrid() {
        return this.grid;
    }

    private boolean isOutOfBound(int index) {
        return index < 0 || index > 2;
    }

    public void place(int i, int j) {
        if (isOutOfBound(i) || isOutOfBound(j) || this.grid[i][j] != ' ') {
            return;
        }

        if (playerX) {
            this.grid[i][j] = 'X';
        } else {
            this.grid[i][j] = 'O';
        }
        this.checkState();
        this.playerX = !this.playerX;
    }

    /**
     * Méthode interne qui vérifie si quelqu'un a gagné
     */
    private void checkState() {
        for (char[] line : this.grid) {
            if (line[0] != ' ' && line[0] == line[1] && line[1] == line[2]) {
                this.gameState = line[0];
                return;
            }
        }
        for (int col = 0; col < 3; col++) {
            if (grid[0][col] != ' ' && grid[0][col] == grid[1][col] && grid[1][col] == grid[2][col]) {
                this.gameState = grid[0][col];
                return;
            }
        }
        if (grid[0][0] != ' ' && grid[0][0] == grid[1][1] && grid[1][1] == grid[2][2]) {
            this.gameState = grid[0][0];
            return;
        }

        if (grid[0][2] != ' ' && grid[0][2] == grid[1][1] && grid[1][1] == grid[2][0]) {
            this.gameState = grid[0][2];
            return;
        }
        this.isFull();
    }

    private void isFull() {
        for (char[] line : grid) {
            for (char cell : line) {
                if(cell == ' ') {
                    return;
                }
            }
        }

        this.gameState = DRAW;
    }

    public Character winState() {
        return gameState;
    }

}
