package co.simplon.alt6.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import co.simplon.alt6.dao.interfaces.DogDao;
import co.simplon.alt6.entity.Dog;
/**
 * Cette classe est une implémentation du AbstractDao, elle implémentera ses 
 * méthodes abstraites pour l'entité Dog et définira les différentes requêtes SQL.
 * Elle pourra également avoir ses méthodes propres au Dog non génériques, alors afin
 * de conserver l'inversion de dépendance, on dira définir ces méthodes supplémentaires
 * dans l'interface DogDao
 */
public class DogDaoFromAbstract extends AbstractDao<Dog> implements DogDao {

    public DogDaoFromAbstract() {
        super(
        "INSERT INTO dog (name,breed,age) VALUES (?,?,?)",    
        "SELECT * FROM dog",
        "SELECT * FROM dog WHERE id=?",
        "UPDATE dog SET name=?,breed=?,age=? WHERE id=?",
        "DELETE FROM dog WHERE id=?"
        );
    }

    @Override
    protected Dog sqlToEntity(ResultSet rs) throws SQLException {
        return new Dog(
            rs.getInt("id"), 
            rs.getString("name"), 
            rs.getString("breed"), 
            rs.getInt("age"));
    }

    @Override
    protected void setEntityId(Dog entity, int id) {
        entity.setId(id);
    }

    @Override
    protected void entityBindValues(PreparedStatement stmt, Dog entity) throws SQLException {
        stmt.setString(1, entity.getName());
        stmt.setString(2, entity.getBreed());
        stmt.setInt(3, entity.getAge());
    }

    @Override
    protected void entityBindValuesWithId(PreparedStatement stmt, Dog entity) throws SQLException {
       entityBindValues(stmt, entity);
       stmt.setInt(4, entity.getId());
    }
    
}
