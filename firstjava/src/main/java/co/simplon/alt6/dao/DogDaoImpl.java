package co.simplon.alt6.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import co.simplon.alt6.dao.interfaces.Dao;
import co.simplon.alt6.dao.interfaces.DogDao;
import co.simplon.alt6.entity.Dog;

public class DogDaoImpl implements Dao<Dog> {

    @Override
    public boolean add(Dog dog) {
        
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO dog (name,breed,age) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, dog.getName());
            stmt.setString(2, dog.getBreed());
            stmt.setInt(3, dog.getAge());
            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            dog.setId(rs.getInt(1));

            return true;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<Dog> getAll() {
        List<Dog> list = new ArrayList<>();
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM dog");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                Dog dog = sqlToDog(rs);
                list.add(dog);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public Dog getById(int id) {
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM dog WHERE id=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                Dog dog = sqlToDog(rs);
                return dog;

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    private Dog sqlToDog(ResultSet rs) throws SQLException {
        return new Dog(
            rs.getInt("id"), 
            rs.getString("name"), 
            rs.getString("breed"), 
            rs.getInt("age")
        );
    }

    @Override
    public boolean update(Dog dog) {
           
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE dog SET name=?,breed=?,age=? WHERE id=?");
            stmt.setString(1, dog.getName());
            stmt.setString(2, dog.getBreed());
            stmt.setInt(3, dog.getAge());
            stmt.setInt(4, dog.getId());

            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean delete(int id) {
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM dog WHERE id=?");
            stmt.setInt(1,id);

            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    
}
