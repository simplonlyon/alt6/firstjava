package co.simplon.alt6.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


/**
 * Cette classe est un exemple de classe abstraite et générique.
 * Abstraite indique qu'elle comporte des méthodes abstraites (signature de méthode sans code)
 * qui devront être implémentées par ses enfants.
 * Générique indique que cette classe pourra avoir un lien fort avec une autre classe
 * (ici une entité) mais que la classe en question est inconnue au moment de faire celle ci
 * Dans ce cas présent on fait une classe abstraite et générique Dao qui contiendra tout
 * le code commun du CRUD des DAO pouvant s'adapter à n'importe quelle entité
 */
public abstract class AbstractDao<E> {
    //On liste les différentes requêtes SQL qui seront utilisées et devront être définies par la classe enfant
    protected String addSQL;
    protected String getAllSQL;
    protected String getByIdSQL;
    protected String updateSQL;
    protected String deleteSQL;


    public AbstractDao(String addSQL, String getAllSQL, String getByIdSQL, String updateSQL, String deleteSQL) {
        this.addSQL = addSQL;
        this.getAllSQL = getAllSQL;
        this.getByIdSQL = getByIdSQL;
        this.updateSQL = updateSQL;
        this.deleteSQL = deleteSQL;
    }

    public List<E> getAll() {
        List<E> list = new ArrayList<>();
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(getAllSQL);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                E entity = sqlToEntity(rs);
                list.add(entity);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public E getById(int id) {
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(getByIdSQL);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                E entity = sqlToEntity(rs);
                return entity;

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public boolean delete(int id) {
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(deleteSQL);
            stmt.setInt(1,id);

            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

      public boolean add(E entity) {
        
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(addSQL, Statement.RETURN_GENERATED_KEYS);
            entityBindValues(stmt,entity);
            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            setEntityId(entity, rs.getInt(1));

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean update(E entity) {
           
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(updateSQL);
            entityBindValuesWithId(stmt,entity);

            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Méthode abstraite qui indiquera comment on assigne un id à l'entité ciblée
     */
    protected abstract void setEntityId(E entity, int id);

    /**
     * Méthode abstraite qui contiendra les différents set... pour assigner des valeurs
     * aux placeholders de la requête add
     */
    protected abstract void entityBindValues(PreparedStatement stmt, E entity)  throws SQLException ;
    /**
     * Même chose que entityBindValues mais avec un set de l'id en plus (pour un update)
     */
    protected abstract void entityBindValuesWithId(PreparedStatement stmt, E entity)  throws SQLException ;
    /**
     * Méthode qui prend une ligne de résultat d'un ResultSet pour le convertir en
     * une instance de l'entité ciblée
     */
    protected abstract E sqlToEntity(ResultSet rs) throws SQLException;


}
